﻿using System;

namespace M3UF4Prova_RodriguezAdrian
{
    class M3UF4Prova_RodriguezAdrian
    {
        public interface IPokemon
        {
            public void Atacar(IPokemon obj);
            public void Curar();
        }

        public abstract class Pokemon : IPokemon
        {
            public string Nombre { get; set; }
            public string Tipo { get; set; }
            public int Nivel { get; set; }
            public int VidaMaxima { get; set; }
            public int VidaActual { get; set; }

            public Pokemon()
            {
                this.Nombre = "pokemon";
                this.Tipo = "Sin tipo";
                this.Nivel = 0;
                this.VidaMaxima = 0;
                this.VidaActual = 0;
            }

            public Pokemon(string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual)
            {
                this.Nombre = nombre;
                this.Tipo = tipo;
                this.Nivel = nivel;
                this.VidaMaxima = vidaMaxima;
                this.VidaActual = vidaActual;
            }

            public abstract int AtaquePoder();

            public void Atacar(IPokemon obj)
            {
                Console.WriteLine($"{this.Nombre} està atacando.");
                ((Pokemon)obj).VidaActual -= this.Nivel;
            }

            public void Curar()
            {
                Console.WriteLine($"{this.Nombre} està curando.");
                if (this.VidaActual > this.VidaMaxima - 3)
                {
                    this.VidaActual = this.VidaMaxima;
                }
                else
                {
                    this.VidaActual += 3;
                }
            }
        }

        public class PokemonAgua : Pokemon
        {
            public int PoderAgua { get; set; }

            public PokemonAgua()
            {
                this.PoderAgua = 0;
            }

            public PokemonAgua(string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual, int poderAgua) : base(nombre, tipo, nivel,vidaMaxima,vidaActual)
            {
                this.PoderAgua = poderAgua;
            }


            public override int AtaquePoder()
            {
                return this.Nivel*this.PoderAgua;
            } 
        }

        public class PokemonFuego : Pokemon
        {
            public int PoderFuego { get; set; }

            public PokemonFuego()
            {
                this.PoderFuego = 0;
            }

            public PokemonFuego(string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual, int poderFuego) : base(nombre, tipo, nivel, vidaMaxima, vidaActual)
            {
                this.PoderFuego = poderFuego;
            }


            public override int AtaquePoder()
            {
                return this.Nivel * this.PoderFuego;
            }
        }

        public class PokemonPlanta : Pokemon
        {
            public int PoderPlanta { get; set; }

            public PokemonPlanta()
            {
                this.PoderPlanta = 0;
            }

            public PokemonPlanta(string nombre, string tipo, int nivel, int vidaMaxima, int vidaActual, int poderplanta) : base(nombre, tipo, nivel, vidaMaxima, vidaActual)
            {
                this.PoderPlanta = poderplanta;
            }


            public override int AtaquePoder()
            {
                return this.Nivel * this.PoderPlanta;
            }
        }


        public class PruebasEstadioPokemon
        {
            static void Main()
            {
                int optionMenu;
                PokemonAgua Piplup = new PokemonAgua("Piplup", "Agua", 5, 20, 20, 2);
                PokemonAgua Charmander = new PokemonAgua("Charmander", "Fuego", 10, 30, 30, 10);
                PokemonAgua Bulbasour = new PokemonAgua("Bulbasour", "Planta", 3, 25, 25, 6);

                Console.WriteLine("¡QUE COMIENCE EL COMBATE POKEMON!");
                Console.WriteLine($"Pokemon en batalla: {Piplup.Nombre} y {Charmander.Nombre}");
                Console.ReadLine();
                Console.Clear();
                do
                {
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("1 - Ataque\n2 - Ataque Poder\n3 - Curar");
                        Console.WriteLine("¿Piplup que Quieres hacer?");
                        optionMenu = Convert.ToInt32(Console.ReadLine());
                        switch (optionMenu)
                        {
                            case 1:
                                Piplup.Atacar(Charmander);
                                if (Charmander.VidaActual <= 0)
                                {
                                    Console.WriteLine($"Vida actual de Charmander: 0\nCHARMANDER HA MUERTO :(");
                                }
                                else
                                {
                                    Console.WriteLine($"Vida actual de Charmander: {Charmander.VidaActual}");
                                }
                                break;
                            case 2:
                                AtaqueElemental(Piplup, Charmander);
                                if (Charmander.VidaActual <= 0)
                                {
                                    Console.WriteLine($"Vida actual de Charmander: 0\nCHARMANDER HA MUERTO :(");
                                }
                                else
                                {
                                    Console.WriteLine($"Vida actual de Charmander: {Charmander.VidaActual}");
                                }
                                break;
                            case 3:
                                Piplup.Curar();
                                Console.WriteLine($"Vida actual de Piplup: {Piplup.VidaActual}");
                                break;
                            default:
                                Console.WriteLine("\nOpción incorrecta!\n");
                                Console.ReadLine();
                                break;
                        }
                    } while (optionMenu != 1 && optionMenu != 2 && optionMenu != 3);
                    if (Charmander.VidaActual > 0)
                    {
                        do
                        {
                            Console.ReadLine();
                            Console.Clear();
                            Console.WriteLine("1 - Ataque\n2 - Ataque Poder\n3 - Curar");
                            Console.WriteLine("¿Charmander que Quieres hacer?");
                            optionMenu = Convert.ToInt32(Console.ReadLine());
                            switch (optionMenu)
                            {
                                case 1:
                                    Charmander.Atacar(Piplup);
                                    if (Piplup.VidaActual <= 0)
                                    {
                                        Console.WriteLine($"Vida actual de Piplup: 0\nPIPLUP HA MUERTO :(");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"Vida actual de Piplup: {Piplup.VidaActual}");
                                    }
                                    break;
                                case 2:
                                    AtaqueElemental(Charmander, Piplup);
                                    if (Piplup.VidaActual <= 0)
                                    {
                                        Console.WriteLine($"Vida actual de Piplup: 0\nPIPLUP HA MUERTO :(");
                                    }
                                    else
                                    {
                                        Console.WriteLine($"Vida actual de Piplup: {Piplup.VidaActual}");
                                    }
                                    break;
                                case 3:
                                    Charmander.Curar();
                                    Console.WriteLine($"Vida actual de Charmander: {Charmander.VidaActual}");
                                    break;
                                default:
                                    Console.WriteLine("\nOpción incorrecta!\n");
                                    break;
                            }
                        } while (optionMenu != 1 && optionMenu != 2 && optionMenu != 3);
                        Console.ReadLine();
                        Console.Clear();
                    }
                } while (Piplup.VidaActual>0 && Charmander.VidaActual>0);

            }
            static void AtaqueElemental(IPokemon atacante, IPokemon defensor)
            {
                Pokemon pokemonAtacante = (Pokemon)atacante;
                Pokemon pokemonDefensor = (Pokemon)defensor;

                if (pokemonAtacante.Tipo == "Agua" && pokemonDefensor.Tipo == "Fuego")
                {
                    Console.WriteLine($"¡El {pokemonAtacante.Nombre} ataca con poder de agua y causa {pokemonAtacante.AtaquePoder()} a {pokemonDefensor.Nombre}!");
                }
                else if (pokemonAtacante.Tipo == "Fuego" && pokemonDefensor.Tipo == "Planta")
                {
                    Console.WriteLine($"¡El {pokemonAtacante.Nombre} ataca con poder de fuego y causa {pokemonAtacante.AtaquePoder()} a {pokemonDefensor.Nombre}!");
                }
                else if (pokemonAtacante.Tipo == "Planta" && pokemonDefensor.Tipo == "Agua")
                {
                    Console.WriteLine($"¡El {pokemonAtacante.Nombre} ataca con poder de planta y causa {pokemonAtacante.AtaquePoder()} a {pokemonDefensor.Nombre}!");
                }
                else
                {
                    Console.WriteLine($"No hay ventaja elemental, el daño causado es el nivel del atacante: {pokemonAtacante.Nivel}");
                }
                pokemonDefensor.VidaActual -= pokemonAtacante.AtaquePoder();
            }


        }
    }
}
